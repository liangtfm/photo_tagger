function PhotosListView() {
  this.$el = $("<div></div>");
};

PhotosListView.prototype.render = function(photos) {
  this.$el = $("<div><ul></ul></div>");
  var $el = this.$el;
  var $ul = $el.find('ul');

  photos.forEach(function(photo) {
    $ul.append("<li>" + photo.attr.title + "</li>")
  })

  $el.append($ul);

  return this.$el;
};